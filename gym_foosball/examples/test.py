import gym
import gym_foosball
from gym_foosball.examples.agents import RandomAgent
import numpy as np

def normalize_observation(observation, min, max):
    return (observation - min) / (max - min)

def denormalize_action(action, min, max):
    return min + action * (max - min)

episodes = 2
timesteps = 100

# Each agent takes 8 observation from foosmen and 9 observation from ball and
# return 8 action for foosmen
blue_agent = RandomAgent(8)
red_agent = RandomAgent(8)
env = gym.make('Foosball-v0')

for episode in range(episodes):
    print('(test) Episode {}/{}'.format(episode + 1, episodes))
    observation = env.reset()
    blue_joint_observation = observation[0:8]
    red_joint_observation = observation[8:16]
    ball_observation = observation[16:25]
    blue_joint_observation[:4] = normalize_observation(blue_joint_observation[:4], -np.pi, np.pi)
    blue_joint_observation[4:] = normalize_observation(blue_joint_observation[4:], -env.max_prism_joint_positions, env.max_prism_joint_positions)
    red_joint_observation[:4] = normalize_observation(red_joint_observation[:4], -np.pi, np.pi)
    red_joint_observation[4:] = normalize_observation(red_joint_observation[4:], -env.max_prism_joint_positions, env.max_prism_joint_positions)

    for timestep in range(timesteps):
        print('(test) Timestep {}/{}'.format(timestep + 1, timesteps))
        env.render()

        # Compute action
        blue_action = blue_agent.act(np.concatenate((blue_joint_observation, ball_observation)))
        red_action = blue_agent.act(np.concatenate((blue_joint_observation, ball_observation)))
        action = np.concatenate((blue_action, red_action))
        action[:4] = denormalize_action(action[:4], -np.pi, np.pi)
        action[4:8] = denormalize_action(action[4:8], -env.max_prism_joint_positions, env.max_prism_joint_positions)
        action[8:12] = denormalize_action(action[8:12], -np.pi, np.pi)
        action[12:] = denormalize_action(action[12:], -env.max_prism_joint_positions, env.max_prism_joint_positions)

        # Take action
        observation, reward, done, info = env.step(action)
        blue_joint_observation = observation[0:8]
        red_joint_observation = observation[8:16]
        ball_observation = observation[16:25]
        # blue_joint_observation = normalize_observation(blue_joint_observation, env.observation_minima, env.observation_maxima)
        # red_joint_observation = normalize_observation(red_joint_observation, env.observation_minima, env.observation_maxima)
        # print('(test) Ball position =', ball_observation[0:3])

        if done:
            print('(test) Episode {} finished after {} timesteps.'.format(episode + 1, timestep + 1))
            if reward[0] == 1:
                print('(test) Blue wins.')
            if reward[1] == 1:
                print('(test) Red wins.')
            break

# Close the environment
env.close()
