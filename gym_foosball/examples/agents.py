import numpy as np

class RandomAgent(object):
    def __init__(self, action_shape):
        self.action_shape = action_shape

    def act(self, observation):
        return np.random.rand(self.action_shape)
