from gym.envs.registration import register

register(
    id='Foosball-v0',
    entry_point='gym_foosball.envs:FoosballEnv',
)

# register(
#     id='Foosball-extrahard-v0',
#     entry_point='gym_foosball.envs:FoosballExtraHardEnv',
# )
