import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym.envs.classic_control import rendering

from vrepper.core import vrepper
from vrepper.utils import blocking, oneshot

import os
import numpy as np

class FoosballEnv(gym.Env):
    """
    Description:
        The aim of the game is to use the control knobs (controlling both angle
        and position) to move the ball into the opponent’s goal.
    Source:
        This environment corresponds to the foosball game for V-REP at
        https://gitlab.com/gvgramazio/foosball-vrep.
    Observation:
        Type: np.ndarray
        Shape: (16,)
        Num    Observation                    Min       Max
        0	   Blue Goalkeeper Angle (rad)    -np.pi    np.pi
        1	   Blue Defender Angle (rad)      -np.pi    np.pi
        2	   Blue Midfielder Angle (rad)    -np.pi    np.pi
        3	   Blue Striker Angle (rad)       -np.pi    np.pi
        4	   Blue Goalkeeper Position       -0.093    0.093
        5	   Blue Defender Position         -0.182    0.182
        6	   Blue Midfielder Position       -0.065    0.065
        7	   Blue Striker Position          -0.11     0.11
        8	   Red Goalkeeper Angle (rad)     -np.pi    np.pi
        9	   Red Defender Angle (rad)       -np.pi    np.pi
        10	   Red Midfielder Angle (rad)     -np.pi    np.pi
        11	   Red Striker Angle (rad)        -np.pi    np.pi
        12	   Red Goalkeeper Position        -0.093    0.093
        13	   Red Defender Position          -0.182    0.182
        14	   Red Midfielder Position        -0.065    0.065
        15	   Red Striker Position           -0.11     0.11
    Actions:
        Type: np.ndarray
        Shape: (16,)
        Num    Action                                Min       Max
        0	   Blue Goalkeeper Target Angle (rad)    -np.pi    np.pi
        1	   Blue Defender Target Angle (rad)      -np.pi    np.pi
        2	   Blue Midfielder Target Angle (rad)    -np.pi    np.pi
        3	   Blue Striker Target Angle (rad)       -np.pi    np.pi
        4	   Blue Goalkeeper Target Position       -0.093    0.093
        5	   Blue Defender Target Position         -0.182    0.182
        6	   Blue Midfielder Target Position       -0.065    0.065
        7	   Blue Striker Target Position          -0.11     0.11
        8	   Red Goalkeeper Target Angle (rad)     -np.pi    np.pi
        9	   Red Defender Target Angle (rad)       -np.pi    np.pi
        10	   Red Midfielder Target Angle (rad)     -np.pi    np.pi
        11	   Red Striker Target Angle (rad)        -np.pi    np.pi
        12	   Red Goalkeeper Target Position        -0.093    0.093
        13	   Red Defender Target Position          -0.182    0.182
        14	   Red Midfielder Target Position        -0.065    0.065
        15	   Red Striker Target Position           -0.11     0.11
    Reward:
        None
    Starting State:
        Revolute Joint: All angles are set to zero.
        Prismatic Joint: All positions are set to zero.
    Episode Termination:
        None
    """
    metadata = {'render.modes': ['human']}

    def __init__(self, headless=True):
        self.venv = vrepper(headless=headless)
        self.venv.start()
        self.venv.load_scene(os.path.dirname(os.path.dirname(__file__)) + '/scenes/foosball.ttt')

        # Retrieve handles
        # TODO: The handles are retrieved without checking the return code. Fix it in the future.
        self.blue_rev_joint_handles = [self.venv.simxGetObjectHandle('Blue_Revolute_joint' + str(i), blocking)[1] for i in range (4)]
        self.blue_prism_joint_handles = [self.venv.simxGetObjectHandle('Blue_Prismatic_joint' + str(i), blocking)[1] for i in range (4)]
        self.red_rev_joint_handles = [self.venv.simxGetObjectHandle('Red_Revolute_joint' + str(i), blocking)[1] for i in range (4)]
        self.red_prism_joint_handles = [self.venv.simxGetObjectHandle('Red_Prismatic_joint' + str(i), blocking)[1] for i in range (4)]
        self.ball_handle = self.venv.simxGetObjectHandle('Ball', blocking)[1]
        self.blue_foosmen_handles = [self.venv.simxGetObjectHandle('Blue_Foosman_dyn' + str(i), blocking)[1] for i in range (11)]
        self.red_foosmen_handles = [self.venv.simxGetObjectHandle('Red_Foosman_dyn' + str(i), blocking)[1] for i in range (11)]

        # Set the ball initial velocity
        # TODO:
        #   - Set the ball initial position
        #   - Use a seed
        ball_initial_velocity = [
            0.02 * np.random.rand(), # x
            2 * np.random.rand(), # y
            0] # z
        self.venv.simxSetObjectFloatParameter(self.ball_handle, 3000, ball_initial_velocity[0], oneshot)
        self.venv.simxSetObjectFloatParameter(self.ball_handle, 3001, ball_initial_velocity[1], oneshot)
        self.venv.simxSetObjectFloatParameter(self.ball_handle, 3002, ball_initial_velocity[2], oneshot)

        self.max_prism_joint_positions = np.array([
            0.9451e-1, # Goalkeeper
            1.82e-1,   # Defense
            0.665e-1,  # Midfield
            1.11e-1])  # Attack

        self.viewer = None # This line is needed to use the rendering function

        self.viewer = None

        print('(foosballEnv) Environment initialized.')

    def get_observation(self):
        # Get the positions of joints. The position is angular for revolute
        # joints and linear for prismatic joints.
        blue_rev_joint_positions = [self.venv.simxGetJointPosition(x, blocking)[1] for x in self.blue_rev_joint_handles]
        blue_prism_joint_positions = [self.venv.simxGetJointPosition(x, blocking)[1] for x in self.blue_prism_joint_handles]
        red_rev_joint_positions = [self.venv.simxGetJointPosition(x, blocking)[1] for x in self.red_rev_joint_handles]
        red_prism_joint_positions = [self.venv.simxGetJointPosition(x, blocking)[1] for x in self.red_prism_joint_handles]

        # Get the position and velocities of the ball.
        ball_position = self.venv.simxGetObjectPosition(self.ball_handle, -1, blocking)[1]
        ball_linear_velocity, ball_angular_velocity = self.venv.simxGetObjectVelocity(self.ball_handle, blocking)[1:]

        # Get the position of foosmen. This information could be retrieved from
        # the joint positions (plus some static infos about the table's
        # dimensions) but it could be helpful to have it directly.
        # Only the y position is taken into consideration because it's the only
        # to change.
        blue_foosmen_positions = [self.venv.simxGetObjectPosition(x, -1, blocking)[1][1] for x in self.blue_foosmen_handles]
        red_foosmen_positions = [self.venv.simxGetObjectPosition(x, -1, blocking)[1][1] for x in self.red_foosmen_handles]

        # The positions of foosmen and ball will be later used by the rendering
        # function.
        self.blue_rev_joint_positions = np.array(blue_rev_joint_positions)
        self.red_rev_joint_positions = np.array(red_rev_joint_positions)
        self.blue_foosmen_positions = np.array(blue_foosmen_positions)
        self.red_foosmen_positions = np.array(red_foosmen_positions)
        self.ball_position = np.array(ball_position)

        observation = np.concatenate((
            blue_rev_joint_positions,   # 4 values
            blue_prism_joint_positions, # 4 values
            red_rev_joint_positions,    # 4 values
            red_prism_joint_positions,  # 4 values
            ball_position,              # 3 values
            ball_linear_velocity,       # 3 values
            ball_angular_velocity,      # 3 values
            blue_foosmen_positions,     # 11 values
            red_foosmen_positions))     # 11 values

        # assert observation.shape == (25, 1), '`observation.shape` different from what expected'
        # observation = np.reshape(observation, 25)
        #print('(foosballEnv) observation =', observation)

        # Check if observation has values outside its thresholds. If they exist
        # there is a problem in the V-REP scene.
        # assert not np.less(observation[0:8], self.observation_minima).any(), '`observation` has at least one value less than expected.'
        # assert not np.greater(observation[0:8], self.observation_maxima).any(), '`observation` has at least one value greater than expected'
        # assert not np.less(observation[8:16], self.observation_minima).any(), '`observation` has at least one value less than expected.'
        # assert not np.greater(observation[8:16], self.observation_maxima).any(), '`observation` has at least one value greater than expected'

        # TODO: Add some check on the returned value.
        return observation

    def step(self, action):
        """
        Parameters
        ----------
        action :

        Returns
        -------
        observation, reward, episode_over, info : tuple
            observation (object) :
                an environment-specific object representing your observation of
                the environment.
            reward (float) :
                amount of reward achieved by the previous action. The scale
                varies between environments, but the goal is always to increase
                your total reward.
            episode_over (bool) :
                whether it's time to reset the environment again. Most (but not
                all) tasks are divided up into well-defined episodes, and done
                being True indicates the episode has terminated. (For example,
                perhaps the pole tipped too far, or you lost your last life.)
            info (dict) :
                 diagnostic information useful for debugging. It can sometimes
                 be useful for learning (for example, it might contain the raw
                 probabilities behind the environment's last state change).
                 However, official evaluations of your agent are not allowed to
                 use this for learning.
        """
       # self.take_action(action)
       # self.status = self.env.step()
       # reward = self.get_reward()
       # ob = self.env.getState()
       # episode_over = self.status != hfo_py.IN_GAME

        # Action
        self.take_action(action)

        # Step
        self.venv.step_blocking_simulation()

        # Observation
        observation = self.get_observation()

        # If there is a goal, the episode is over.
        reward, episode_over = self.check_goal()

        # Additional info
        info = {}

        return observation, reward, episode_over, info

    def reset(self):
        self.venv.stop_simulation()
        self.venv.start_blocking_simulation()
        observation = self.get_observation()

        return observation

    def render(self, mode='human', close=False):
        screen_width = 600
        screen_height = 400

        world_width = 0.8
        world_height = 1.2

        scale = min(screen_width / world_width, screen_height / world_height)

        ball_radius = scale * 3.5e-2
        foosball_field_width = scale * 0.71
        foosball_field_height = scale * 1.15
        foosman_width = scale * 3.7e-2
        foosman_height =  scale * 2.6e-2


        if self.viewer is None:
            self.viewer = rendering.Viewer(screen_width, screen_height)

            l, r, t, b = -foosball_field_width/2, foosball_field_width/2, foosball_field_height/2, -foosball_field_height/2
            foosball_field = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            foosball_field.set_color(.2,.7,.2)
            self.foosball_field_trans = rendering.Transform()
            foosball_field.add_attr(self.foosball_field_trans)
            self.viewer.add_geom(foosball_field)

            l, r, t, b = -ball_radius/2, ball_radius/2, ball_radius/2, -ball_radius/2
            ball = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            self.ball_trans = rendering.Transform()
            ball.add_attr(self.ball_trans)
            self.viewer.add_geom(ball)

        # The foosmen definitions are redone each time because their height
        # could change at each timestep
        blue_foosmen = []
        red_foosmen = []
        self.blue_foosmen_trans = []
        self.red_foosmen_trans = []
        # TODO: This is one of the ugliest assignment I've ever done. It
        # should be written better.
        blue_foosmen_additional_heights = np.empty(11)
        red_foosmen_additional_heights = np.empty(11)
        for i in range(11):
            if i == 0:
                blue_foosmen_additional_heights[i] = scale * 5.5e-2 * np.sin(self.blue_rev_joint_positions[0])
                red_foosmen_additional_heights[i] = scale * 5.5e-2 * np.sin(self.red_rev_joint_positions[0])
            elif i < 3:
                blue_foosmen_additional_heights[i] = scale * 5.5e-2 * np.sin(self.blue_rev_joint_positions[1])
                red_foosmen_additional_heights[i] = scale * 5.5e-2 * np.sin(self.red_rev_joint_positions[1])
            elif i < 8:
                blue_foosmen_additional_heights[i] = scale * 5.5e-2 * np.sin(self.blue_rev_joint_positions[2])
                red_foosmen_additional_heights[i] = scale * 5.5e-2 * np.sin(self.red_rev_joint_positions[2])
            else:
                blue_foosmen_additional_heights[i] = scale * 5.5e-2 * np.sin(self.blue_rev_joint_positions[3])
                red_foosmen_additional_heights[i] = scale * 5.5e-2 * np.sin(self.red_rev_joint_positions[3])
        for i in range(11):
            t = foosman_height/2 + blue_foosmen_additional_heights[i] if blue_foosmen_additional_heights[i] > 0 else foosman_height/2
            b = -foosman_height/2 + blue_foosmen_additional_heights[i] if blue_foosmen_additional_heights[i] < 0 else -foosman_height/2
            l, r = -foosman_width/2, foosman_width/2
            blue_foosmen.append(rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)]))
            blue_foosmen[i].set_color(.2,.2,.8)
            self.blue_foosmen_trans.append(rendering.Transform())
            blue_foosmen[i].add_attr(self.blue_foosmen_trans[i])
            self.viewer.add_onetime(blue_foosmen[i])
        for i in range(11):
            t = foosman_height/2 + red_foosmen_additional_heights[i] if red_foosmen_additional_heights[i] > 0 else foosman_height/2
            b = -foosman_height/2 + red_foosmen_additional_heights[i] if red_foosmen_additional_heights[i] < 0 else -foosman_height/2
            l, r = -foosman_width/2, foosman_width/2
            red_foosmen.append(rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)]))
            red_foosmen[i].set_color(.8,.2,.2)
            self.red_foosmen_trans.append(rendering.Transform())
            red_foosmen[i].add_attr(self.red_foosmen_trans[i])
            self.viewer.add_onetime(red_foosmen[i])

        foosball_field_x = screen_width / 2
        foosball_field_y = screen_height / 2
        self.foosball_field_trans.set_translation(foosball_field_x, foosball_field_y)

        blue_foosmen_y = [
            -5.2500e-1,
            -2.2500e-1,
            -2.2500e-1,
            +7.5000e-2,
            +7.5000e-2,
            +7.5000e-2,
            +7.5000e-2,
            +7.5000e-2,
            +3.7500e-1,
            +3.7500e-1,
            +3.7500e-1]
        red_foosmen_y = [
            +5.2500e-1,
            +2.2500e-1,
            +2.2500e-1,
            -7.5000e-2,
            -7.5000e-2,
            -7.5000e-2,
            -7.5000e-2,
            -7.5000e-2,
            -3.7500e-1,
            -3.7500e-1,
            -3.7500e-1]
        blue_foosmen_x = []
        red_foosmen_x = []
        for i in range(11):
            blue_foosmen_x.append(scale * self.blue_foosmen_positions[i] + screen_width / 2.0)
            red_foosmen_x.append(scale * self.red_foosmen_positions[i] + screen_width / 2.0)
            blue_foosmen_y[i] = scale * blue_foosmen_y[i] + screen_height / 2.0
            red_foosmen_y[i] = scale * red_foosmen_y[i] + screen_height / 2.0
            self.blue_foosmen_trans[i].set_translation(blue_foosmen_x[i], blue_foosmen_y[i])
            self.red_foosmen_trans[i].set_translation(red_foosmen_x[i], red_foosmen_y[i])

        ball_x = scale * self.ball_position[1] + screen_width / 2.0
        ball_y = scale * self.ball_position[0] + screen_height / 2.0
        self.ball_trans.set_translation(ball_x, ball_y)

        return self.viewer.render(return_rgb_array = mode == 'rgb_array')

    def close(self):
        if self.viewer:
            self.viewer.close()
            self.viewer = None

    def take_action(self, action):
        # Check if action has the correct shape
        assert action.shape == (16,), '`action.shape` different from what expected'

        # Check for values outside ranges. If present, don't trow an error but
        # simply replace them with the corresponding thresholds.
        np.clip(a=action[4:8], a_min=-self.max_prism_joint_positions, a_max=self.max_prism_joint_positions, out=action[4:8])
        np.clip(a=action[12:], a_min=-self.max_prism_joint_positions, a_max=self.max_prism_joint_positions, out=action[12:])

        for i in range(4):
            self.venv.simxSetJointTargetPosition(self.blue_rev_joint_handles[i], action[i], blocking)
            self.venv.simxSetJointTargetPosition(self.blue_prism_joint_handles[i], action[i+4], blocking)
            self.venv.simxSetJointTargetPosition(self.red_rev_joint_handles[i], action[i+8], blocking)
            self.venv.simxSetJointTargetPosition(self.red_prism_joint_handles[i], action[i+12], blocking)

    def check_goal(self):
        reward = np.zeros(2)
        episode_over = False

        # Check y position to see if ther is a goal
        if self.ball_position[0] < -0.575:
            reward[0] = 1
            reward[1] = -1
            episode_over = True
        elif self.ball_position[0] > 0.575:
            reward[0] = -1
            reward[1] = 1
            episode_over = True

        return reward, episode_over
