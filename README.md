# gym-foosball
This repository contains a PIP package which is an OpenAI environment for simulations through V-REP.

## Prerequisites
Some dependencies will be solved automatically during installation of this package. Other ones should be solved manually.

This package requires V-REP and is tested with version 3.5. You can download the last version of V-REP [here][1]. For further information about V-REP visit http://www.coppeliarobotics.com/.

After the installation, please add your V-REP installation to your `PATH` variable:

- (Windows) might be something like `C:/Program Files/V-REP3/V-REP_PRO_EDU/`
   ```bash
   $ set PATH=%PATH%;C:/Program Files/V-REP3/V-REP_PRO_EDU/
   ```
- (Mac OS X) might be something like `/Users/USERNAME/V-REP_PRO_EDU/vrep.app/Contents/MacOS/`
   ```bash
   $ export PATH=$PATH:"/Users/USERNAME/V-REP_PRO_EDU/vrep.app/Contents/MacOS/"
   ```
- (Linux) might be something like `/home/USERNAME/tools/V-REP_PRO_EDU_V3_4_0_Linux`
   ```bash
   $ export PATH="/home/USERNAME/tools/V-REP_PRO_EDU_V3_4_0_Linux":$PATH
   ```

You also need `vrepper` to use this package. Currently I forked version of a forked version of the original one. You can clone it from [here][2].

## Usage
Run the following:

```bash
git clone https://github.com/gvgramazio/gym-foosball
cd gym-foosball
python3 -m pip install -e .
ipython3 /gym_foosball/examples/test.py
```
The last command will start a test of the package.

[1]: http://www.coppeliarobotics.com/downloads.html
[2]: https://github.com/gvgramazio/vrepper
