from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='gym_foosball',
    version='0.0.1',
    author="Giuseppe Valerio Gramazio",
    author_email="gvgramazio@gmail.com",
    description="A OpenAI Gym environment to simulate a foosball game.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/gvgramazio/gym-foosball",
    install_requires=['gym']
    # And any other dependencies foosball needs
)
